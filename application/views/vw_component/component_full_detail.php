<div class="col-md-12" id="dashboard">
<?php foreach ($listbyID as $key => $value): ?>
	<div class="section-title"><?php echo $value['component_name']; ?></div>
	<div class="content-box">
		<div class="panel">
			<div class="panel-body">
			<div class="content-title">Component Details</div>
				<div class="col-md-4">
					<div class="img-container">
						<img src="http://placehold.it/360x360" alt="" class="img-responsive">
					</div>
				</div>
				<div class="col-md-8">
					<div class="component-full-box">
						<div class="component-details"><span class="title-box">Barcode ID: </span> <?php echo $value['component_barcode']; ?></div>
						<div class="component-details"><span class="title-box">Reference Barcode: </span> <?php echo $value['reference_barcode']; ?></div>
						<div class="component-details"><span class="title-box">Type: </span> //component type</div>
						<div class="component-details"><span class="title-box">Material: </span> <?php echo $value['component_material']; ?></div>
						<div class="component-details"><span class="title-box">Dimension(LWH): </span> <?php echo $value['component_dimension']; ?></div>
						<div class="component-details"><span class="title-box">Description: </span> <?php echo $value['component_description']; ?></div>
						<div class="component-details"><span class="title-box"><a href="#"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 3D Drawing - <?php echo $value['component_3d_drawing']; ?></a></div>
						<div class="component-details nb"><span class="title-box"><a href="#"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 2D Drawing - <?php echo $value['component_2d_drawing']; ?></a></div>
					</div>
				</div>
			</div>
		</div> 
	</div>
	<div class="content-box">
		<div class="panel">
			<div class="panel-body">
				<div class="content-title">Stocks Info</div>
				<div class="row">
					<div class="col-md-6">
						<div class="component-details"><span class="title-box">Unit: </span> <?php echo $value['component_unit']; ?></div>
						<div class="component-details"><span class="title-box">Quantity per Order: </span> <?php echo $value['component_qty_porder']; ?></div>
						<div class="component-details"><span class="title-box">Price in US(pcs): &#36;</span><?php echo $value['component_us_price_ppcs']; ?></div>
						<div class="component-details nb"><span class="title-box">Price in Peso(pcs): &#8369;</span><?php echo $value['component_rp_price']; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content-box">
		<div class="panel">
			<div class="panel-body">
				<div class="content-title">Manufacturer</div>
				<div class="row">
					<div class="col-md-6">
						<div class="component-details"><span class="title-box">Name: </span><?php echo $value['manufacturer_name']; ?></div>
						<div class="component-details"><span class="title-box">Address: </span> <?php echo $value['manufacturer_addrs']; ?></div>
						<div class="component-details"><span class="title-box">Country: </span> <?php echo $value['manufacturer_country']; ?></div>
						<div class="component-details"><span class="title-box">Zip Code: </span> <?php echo $value['manufacturer_zip']; ?></div>
					</div>
					<div class="col-md-6">
						<div class="component-details"><span class="title-box">Person In-charge: </span> <?php echo $value['manufacturer_incharge']; ?></div>
						<div class="component-details"><span class="title-box">Designation: </span> <?php echo $value['manufacturer_designation']; ?></div>
						<div class="component-details"><span class="title-box">Email: </span> <?php echo $value['manufacturer_email']; ?></div>
						<div class="component-details"><span class="title-box">Phone Nos.: </span> <?php echo $value['manufacturer_phone_nos']; ?></div>
						<div class="component-details"><span class="title-box">Fax Nos.: </span> <?php echo $value['manufacturer_fax_nos']; ?></div>
						<div class="component-details nb"><span class="title-box">Payment Method: </span> <?php echo $value['manufacturer_paymentmode']; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endforeach ?>
</div>