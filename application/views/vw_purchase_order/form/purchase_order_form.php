<div class="col-md-12" id="dashboard">
	<form action="">
		<div class="section-title">Purchase order</div>
		<div class="content-box">
			<div class="content-wrapper">
				<div class="content-title">Create Purchase Order</div>
				<form action="#" class="form-inline">
					<div class="from-group">
						<label for="manufacturer">Select Manufacturer</label>
					</div>
				</form>
			</div>
		</div>
	</form>
</div>


<?php /* output form for purchase order
<div class="nv3d-form">
	<h1 class="text-center">3D FABLAB PHILS., INC.</h1>
	<div class="address text-center">Unit 309, 3/F The Gateway Centre Paseo De Magallanes, Magallanes Village, Makati City.</div>
	<div class="contacts text-center">
		<p><strong>TIN:</strong> 009-066-388-000</p>
		<p><strong>Telephone Nos:</strong> (02) 851-0412</p>
		<p><strong>Fax:</strong> 851-0109</p>
		<p><strong>Email:</strong> //insert employee email here</p>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered">
			<tr>
				<th class="form-title text-center" colspan="9">Purchased Order</th>
			</tr>
			<tr>
				<td colspan="3">Supplier Name:</td>
				<td colspan="3">E-mail:</td>
				<td colspan="3">Date:</td>
			</tr>
			<tr>
				<td colspan="3">Address:</td>
				<td colspan="3">Contact Person:</td>
				<td colspan="3">Telephone Nos:</td>
			</tr>
			<tr>
				<td colspan="4">Mode of Payment:</td>
				<td colspan="5">Terms of Payment:</td>
			</tr>
			<tr>
				<th>Ref. Image</th>
				<th>Barcode</th>
				<th>Component Name</th>
				<th>Material</th>
				<th>Dimension</th>
				<th>Unit</th>
				<th>Qty</th>
				<th>Price</th>
				<th>TOTAL</th>
			</tr>
			<tr>
				<?php //foreach ($variable as $key => $value): ?>
					<td>1</td>
					<td>2</td>
					<td>3</td>
					<td>4</td>
					<td>5</td>
					<td>6</td>
					<td>7</td>
					<td>8</td>
					<td>9</td>
				<?php //endforeach ?>
			</tr>
			<tr>
				<td colspan="7">Delivery to:</td>
				<td class="text-right"><strong>GRAND TOTAL</strong></td>
				<th><strong>$ XX,XXX.XX</strong></th>
			</tr>
			<tr>
				<td colspan="9">Expected Time of Delivery:</td>
			</tr>
			<tr>
				<td colspan="9" class="text-center text-uppercase"><strong>Terms and Condition</strong></td>
			</tr>
			<tr>
				<td colspan="9">1. Buyer reserves the right to inspect goods. <br/> 2. Buyer reserves the right to cancel <strong>P.O</strong> if delivery date, product specification and terms are not met. <br/> 3. Payment will be processed upon presentation of <strong>Original P.O</strong> and <strong>Sales Invoice</strong>. </td>
			</tr>
			<tr>
				<td colspan="4">
					<p>Prepared By:</p>
					<p>Employee Name <br/> <small>Designation / <?php echo date('Y-m-d H:i:s'); ?></small></p>
				</td>
				<td colspan="5">
					<p>Approved By:</p>
					<p>Employee Name <br/> <small>Designation / // echo date approval</small></p>
				</td>
				
			</tr>
		</table>
	</div>
</div>
*/ ?>