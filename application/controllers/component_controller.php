<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Component_controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->_init();
	}

	private function _init()
	{
		$this->output->set_template('fullwidth-sidebar');
		$this->load->js('assets/themes/default/hero_files/bootstrap-modal.js');
		$this->load->js('assets/themes/novio/js/rpm-alert.js');
	}

	public function index()
	{
		$this->load->model('Component_model');
		$data['component_type_list'] = $this->Component_model->get_allComponentType();
		$data['component_list'] = $this->Component_model->get_allComponent();

		$this->load->model('manufacturer_model');
		$data['manufacturer_list'] = $this->manufacturer_model->get_allManufacturer();

		$this->output->set_common_meta('Components | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->section('sidebar', 'sidebar_panel/sidebar_admin');
		$this->load->view('vw_component/component', $data);
	}

	public function component_type(){
		$this->load->model('Component_model');
		$data['component_type_list'] = $this->Component_model->get_allComponentType();

		$this->output->set_common_meta('Components Type | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->section('sidebar', 'sidebar_panel/sidebar_admin');
		$this->load->view('vw_component/component_type', $data);
	}

	public function add_component_type(){
		$this->load->model('Component_model');

		$cc_alias = strtoupper($this->input->post('cc_alias'));
		$cc_alias_val = $this->Component_model->validate_componentAlias($cc_alias);

		if ($cc_alias_val == "true") {
			$this->session->set_flashdata('messageErr', 'Alias already in used.');
			redirect(base_url('component_controller/component_type'), 'refresh');
		}
		else{
			$data = array(
			'component_type_name' => $this->input->post('cc_name'),
			'component_type_alias' => $cc_alias
			);

			$this->Component_model->add_componentType($data);
			$this->session->set_flashdata('messageSuc', 'New Component type added');
			redirect(base_url('component_controller/component_type'), 'refresh');
		} 
	}

	function add_component(){

		$this->load->model('Component_model');
		$component_type_id = $this->input->post('c_type');
		$data['bcode_type_alias'] = $this->Component_model->get_componenttype_alias($component_type_id);
		$bcode_type_count = $this->Component_model->get_component_lastcount();
		$temp_bcode = $bcode_type_count+1;
		$component_barcode = $data['bcode_type_alias']->component_type_alias.''.str_pad($temp_bcode, 4, '0', STR_PAD_LEFT);

		$this->load->library('upload');
		$number_of_files_uploaded = count($_FILES['upl_files']['name']);
		$file_uploaded = array();
		$file_uploaded_per_name = array();

		for ($i=0; $i < $number_of_files_uploaded; $i++) { 
			$_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
		    $_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
		    $_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
		    $_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
		    $_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];
		     $config = array(
		        'allowed_types' => '*',
		        'max_size'      => 9000,
		        'overwrite'     => FALSE,
		        'upload_path'
		            => './uploads/'
		      );
		    $this->upload->initialize($config);

		     if (! $this->upload->do_upload()) {
		     	 $error = array('error' => $this->upload->display_errors());
		     	 print_r($error);
		     }
		     else{
		     	$file_uploaded[] = $this->upload->data();
		     }
		}

		$drawing3 = $file_uploaded[0]['file_name'];
		$drawing2 = $file_uploaded[1]['file_name'];
		$content_img = $file_uploaded[2]['file_name'];

		$data = array(
			'component_barcode' => $component_barcode,
			'component_name' => $this->input->post('c_name'),
			'component_material' => $this->input->post('c_material'),
			'component_type_id' => $this->input->post('c_type'),
			'component_description' => $this->input->post('c_description'),
			'component_dimension' => $this->input->post('c_dimension'),
			'manufacturer_id' => $this->input->post('supplier_id'),
			'component_3d_drawing' => $drawing3,
			'component_2d_drawing' => $drawing2,
			'component_unit' => $this->input->post('c_unit'),
			'component_qty_porder' => $this->input->post('qty_per_ordr'),
			'component_prod_stock' => $this->input->post('c_pstock'),
			'component_us_price_ppcs' => $this->input->post('c_us_pricep'),
			'component_rp_price' => $this->input->post('c_rp_price'),
			'component_img' => $content_img,
			'component_remarks' => $this->input->post('c_rermarks'),
			'reference_barcode' => $this->input->post('ref_barcode')
			);

		$this->Component_model->add_component($data);
		redirect(base_url('component_controller'), 'refresh'); 
	}

	public function full_list_component(){	

		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$cmp_id = $_GET['id'];

		$this->load->model('Component_model');
		$data['listbyID'] = $this->Component_model->get_component_byId($cmp_id);

		$this->output->set_common_meta('Component Details | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->section('sidebar', 'sidebar_panel/sidebar_admin');
		$this->load->view('vw_component/component_full_detail', $data);
	}	
}