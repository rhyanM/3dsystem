<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_order_controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->_init();
	}

	private function _init()
	{
		$this->output->set_template('fullwidth-sidebar');
		$this->load->js('assets/themes/default/hero_files/bootstrap-modal.js');
		$this->load->js('assets/themes/novio/js/rpm-alert.js');
	}

	public function index()
	{
		$this->output->set_common_meta('Purchase Order | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->section('sidebar', 'sidebar_panel/sidebar_admin');
		$this->load->view('vw_purchase_order/purchase_order');
	}

	public function purchase_order_form(){
		$this->output->set_common_meta('Purchase Order Form | 3DFABLAB SYSTEM', ' ', ' ');
		$this->load->section('sidebar', 'sidebar_panel/sidebar_admin');
		$this->load->view('vw_purchase_order/form/purchase_order_form');
	}
}