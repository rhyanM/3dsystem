/*
SQLyog Enterprise - MySQL GUI v7.02 
MySQL - 5.5.5-10.1.13-MariaDB : Database - db_3dsystem
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_3dsystem` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_3dsystem`;

/*Table structure for table `tbl_component` */

DROP TABLE IF EXISTS `tbl_component`;

CREATE TABLE `tbl_component` (
  `component_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `component_barcode` varchar(7) DEFAULT NULL,
  `component_name` varchar(125) DEFAULT NULL,
  `component_material` varchar(125) DEFAULT NULL,
  `component_description` text,
  `component_dimension` text,
  `component_type_id` int(4) DEFAULT NULL,
  `manufacturer_id` int(4) DEFAULT NULL,
  `component_3d_drawing` varchar(125) DEFAULT NULL,
  `component_2d_drawing` varchar(125) DEFAULT NULL,
  `component_unit` varchar(125) DEFAULT NULL,
  `component_qty_porder` int(125) DEFAULT NULL,
  `component_prod_stock` int(125) DEFAULT NULL,
  `component_us_price_ppcs` double(10,2) DEFAULT NULL,
  `component_rp_price` double(10,2) DEFAULT NULL,
  `component_img` varchar(125) DEFAULT NULL,
  `component_remarks` tinytext,
  `reference_barcode` varchar(125) DEFAULT NULL,
  PRIMARY KEY (`component_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_component` */

insert  into `tbl_component`(`component_id`,`component_barcode`,`component_name`,`component_material`,`component_description`,`component_dimension`,`component_type_id`,`manufacturer_id`,`component_3d_drawing`,`component_2d_drawing`,`component_unit`,`component_qty_porder`,`component_prod_stock`,`component_us_price_ppcs`,`component_rp_price`,`component_img`,`component_remarks`,`reference_barcode`) values (0001,'SPC0001','Aluminum Spacer','Alluminum','Color: silver','6.35 x 5.3 x 8 mm',1,1,'mfhandler.jpg',NULL,'pcs',160,640,0.05,5.00,NULL,'No remarks','1'),(0002,'PLT0002','Eccentric Spacer','Stainless Steel','color: silver','10(hex) x 8.85mm',6,2,NULL,NULL,'pieces',100,320,0.21,39.00,NULL,'No Remarks','2'),(0003,'PLT0002','2 Hole Joining Strip Plate','Alluminum','color: silver','40 x 20 x 4 mm',6,3,NULL,NULL,'pcs',120,500,0.80,19.00,NULL,'No remarks','3'),(0004,'BRG0002','Ball Bearing 625 2RS','Metal','color: silver','5 x 16 x 5mm',2,2,NULL,NULL,'pcs',15,50,1.45,89.00,NULL,'No Remarks','13');

/*Table structure for table `tbl_component_type` */

DROP TABLE IF EXISTS `tbl_component_type`;

CREATE TABLE `tbl_component_type` (
  `component_type_id` int(4) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `component_type_name` varchar(12) DEFAULT NULL,
  `component_type_alias` char(3) DEFAULT NULL,
  PRIMARY KEY (`component_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_component_type` */

insert  into `tbl_component_type`(`component_type_id`,`component_type_name`,`component_type_alias`) values (0001,'Spacer','SPC'),(0002,'Bearing','BRG'),(0003,'Bolts','BLT'),(0004,'Bracket','BKT'),(0005,'Washer','WSH'),(0006,'Plate','PLT');

/*Table structure for table `tbl_manufacturer` */

DROP TABLE IF EXISTS `tbl_manufacturer`;

CREATE TABLE `tbl_manufacturer` (
  `manufacturer_id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `manufacturer_name` varchar(255) DEFAULT NULL,
  `manufacturer_addrs` text,
  `manufacturer_zip` int(9) DEFAULT NULL,
  `manufacturer_country` varchar(255) DEFAULT 'Not Specified',
  `manufacturer_phone_nos` int(15) DEFAULT NULL,
  `manufacturer_fax_nos` int(10) DEFAULT NULL,
  `manufacturer_email` varchar(255) DEFAULT 'Not Specified',
  `manufacturer_incharge` varchar(255) DEFAULT 'Not Specified',
  `manufacturer_designation` varchar(255) DEFAULT 'Not Specified',
  `manufacturer_paymentmode` varchar(125) DEFAULT NULL,
  PRIMARY KEY (`manufacturer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_manufacturer` */

insert  into `tbl_manufacturer`(`manufacturer_id`,`manufacturer_name`,`manufacturer_addrs`,`manufacturer_zip`,`manufacturer_country`,`manufacturer_phone_nos`,`manufacturer_fax_nos`,`manufacturer_email`,`manufacturer_incharge`,`manufacturer_designation`,`manufacturer_paymentmode`) values (00001,'Changzhou Wantai Electrical Appliance Co. Ltd','No. 1 Zhengda Road Oishuya Ar',6000001,'China',2147483647,2147483647,'neema17motors@gmail.com','Ms. michelle zong','Owner','Cash on Delivery'),(00002,'Hans Infinite Tools','8007 Pioneer Street',1243,'Philippines',2147483647,2147483647,'hans.infinite@gmail.com','Hans Infinite','Owner','Installment - 7days'),(00003,'AliExpress-Stronghold bolts and Nuts Center','435 E. Edsa Rotonda, Metro Manila',1243,'Philippines',8891220,2616280,'strongholdfasteners@yahoo.com','Kristine','Sales Coordinator','Cash on Delivery');

/*Table structure for table `vws_component_list` */

DROP TABLE IF EXISTS `vws_component_list`;

/*!50001 DROP VIEW IF EXISTS `vws_component_list` */;
/*!50001 DROP TABLE IF EXISTS `vws_component_list` */;

/*!50001 CREATE TABLE `vws_component_list` (
  `component_id` int(4) unsigned zerofill NOT NULL,
  `component_barcode` varchar(7) DEFAULT NULL,
  `component_name` varchar(125) DEFAULT NULL,
  `component_description` text,
  `component_3d_drawing` varchar(125) DEFAULT NULL,
  `component_2d_drawing` varchar(125) DEFAULT NULL,
  `component_unit` varchar(125) DEFAULT NULL,
  `component_qty_porder` int(125) DEFAULT NULL,
  `component_prod_stock` int(125) DEFAULT NULL,
  `component_us_price_ppcs` double(10,2) DEFAULT NULL,
  `component_rp_price` double(10,2) DEFAULT NULL,
  `component_img` varchar(125) DEFAULT NULL,
  `component_remarks` tinytext,
  `component_type_name` varchar(12) DEFAULT NULL,
  `component_material` varchar(125) DEFAULT NULL,
  `reference_barcode` varchar(125) DEFAULT NULL,
  `component_dimension` text,
  `manufacturer_name` varchar(255) DEFAULT NULL,
  `manufacturer_addrs` text,
  `manufacturer_zip` int(9) DEFAULT NULL,
  `manufacturer_country` varchar(255) DEFAULT NULL,
  `manufacturer_phone_nos` int(15) DEFAULT NULL,
  `manufacturer_fax_nos` int(10) DEFAULT NULL,
  `manufacturer_email` varchar(255) DEFAULT NULL,
  `manufacturer_incharge` varchar(255) DEFAULT NULL,
  `manufacturer_designation` varchar(255) DEFAULT NULL,
  `manufacturer_paymentmode` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 */;

/*View structure for view vws_component_list */

/*!50001 DROP TABLE IF EXISTS `vws_component_list` */;
/*!50001 DROP VIEW IF EXISTS `vws_component_list` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vws_component_list` AS select `tbl_component`.`component_id` AS `component_id`,`tbl_component`.`component_barcode` AS `component_barcode`,`tbl_component`.`component_name` AS `component_name`,`tbl_component`.`component_description` AS `component_description`,`tbl_component`.`component_3d_drawing` AS `component_3d_drawing`,`tbl_component`.`component_2d_drawing` AS `component_2d_drawing`,`tbl_component`.`component_unit` AS `component_unit`,`tbl_component`.`component_qty_porder` AS `component_qty_porder`,`tbl_component`.`component_prod_stock` AS `component_prod_stock`,`tbl_component`.`component_us_price_ppcs` AS `component_us_price_ppcs`,`tbl_component`.`component_rp_price` AS `component_rp_price`,`tbl_component`.`component_img` AS `component_img`,`tbl_component`.`component_remarks` AS `component_remarks`,`tbl_component_type`.`component_type_name` AS `component_type_name`,`tbl_component`.`component_material` AS `component_material`,`tbl_component`.`reference_barcode` AS `reference_barcode`,`tbl_component`.`component_dimension` AS `component_dimension`,`tbl_manufacturer`.`manufacturer_name` AS `manufacturer_name`,`tbl_manufacturer`.`manufacturer_addrs` AS `manufacturer_addrs`,`tbl_manufacturer`.`manufacturer_zip` AS `manufacturer_zip`,`tbl_manufacturer`.`manufacturer_country` AS `manufacturer_country`,`tbl_manufacturer`.`manufacturer_phone_nos` AS `manufacturer_phone_nos`,`tbl_manufacturer`.`manufacturer_fax_nos` AS `manufacturer_fax_nos`,`tbl_manufacturer`.`manufacturer_email` AS `manufacturer_email`,`tbl_manufacturer`.`manufacturer_incharge` AS `manufacturer_incharge`,`tbl_manufacturer`.`manufacturer_designation` AS `manufacturer_designation`,`tbl_manufacturer`.`manufacturer_paymentmode` AS `manufacturer_paymentmode` from ((`tbl_component` join `tbl_component_type` on((`tbl_component`.`component_type_id` = `tbl_component_type`.`component_type_id`))) join `tbl_manufacturer` on((`tbl_component`.`manufacturer_id` = `tbl_manufacturer`.`manufacturer_id`))) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
